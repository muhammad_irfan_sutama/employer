import { Component } from '@angular/core';
import {Location} from '@angular/common';
import { numericDirective } from '../numeric.directive';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-employee',
  standalone: true,
  imports: [numericDirective,ReactiveFormsModule],
  templateUrl: './add-employee.component.html',
  styleUrl: './add-employee.component.css'
})
export class AddEmployeeComponent {
  selectedGroup: string = '';
  groupList : string [] = ["alpha", "bravo", "charlie", "delta", "echo", "fanta", "golf", "hotel", "india", "juliet"]
  today = new Date().toISOString().split("T")[0]
  showGroupDropdown: boolean = false;
  validationError : any = []
  employeeForm: FormGroup = new FormGroup({
    userName: new FormControl("",[Validators.required]),
    firstName: new FormControl("",[Validators.required]),
    lastName: new FormControl("",[Validators.required]),
    email: new FormControl("",[Validators.required]),
    birthDate: new FormControl("",[Validators.required]),
    basicSalary: new FormControl("",[Validators.required]),
    group: new FormControl("",[Validators.required]),
  });
  isFormSubmitted: boolean = false;
  constructor(private _location: Location) 
  {}
  onSubmit() {
    this.getFormValidationErrors() 
    if (this.employeeForm.valid) {
      this.backClicked()
    }
  }
  backClicked() {
    this._location.back();
  }
  toggleDropdown() {
    this.showGroupDropdown = !this.showGroupDropdown
  }
  groupSelect(group:string){
    this.selectedGroup = group;
    this.employeeForm.patchValue({
      group: group
    })
    this.toggleDropdown();
  }
  getFormValidationErrors() {
    let validation: any = {}
    Object.keys(this.employeeForm.controls).forEach(key => {
      const controlErrors: any = this.employeeForm.get(key)?.errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
         validation[key] = keyError  
        });
      }
    });
    this.validationError = validation
  }
}
