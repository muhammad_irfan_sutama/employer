import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

export const routes: Routes = [
    { path: '', redirectTo: 'employee', pathMatch: 'full'},
    { path: 'login', component: LoginComponent},
    { path: 'employee', component: HomeComponent,
        children: [
            {
                path: '',
                component: EmployeeListComponent
            },
            {
                path: 'detail/:employeeId',
                component: EmployeeDetailComponent
            },
            {
                path: 'add',
                component: AddEmployeeComponent,
            },
        ],
    },

];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  
  export class AppRoutingModule { }
