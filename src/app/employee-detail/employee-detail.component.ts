import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-employee-detail',
  standalone: true,
  imports: [],
  templateUrl: './employee-detail.component.html',
  styleUrl: './employee-detail.component.css'
})
export class EmployeeDetailComponent implements OnInit{
  employeeData : employee = new employee();
  IDR = new Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR',
  });
  constructor(private _location: Location, private route: ActivatedRoute) 
  {}
  ngOnInit(): void {
    this.route.queryParams.subscribe((params:any) => {
      this.employeeData = params
    });
  }
  backClicked() {
    this._location.back();
  }
}
export class employee {
  userName:string = ""
  firstName: string = "";
  lastName: string = "";
  email: string = "";
  birthDate: string = "";
  basicSalary: number = 0;
  status: string = "";
  group: string = "";
  description: string = "";
}
