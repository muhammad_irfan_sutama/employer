import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-employee-list',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './employee-list.component.html',
  styleUrl: './employee-list.component.css'
})
export class EmployeeListComponent implements OnInit{
  employeeList : employee [] = []
  sortState: sortState = new sortState();
  displayedEmployeList : employee [] = []
  showModal: boolean = false;
  modalData: modalData = new modalData();
  searchValue: string = "";
  debounceTimer :any;
  paginationState : any;
  IDR = new Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR',
  });
  constructor(private router: Router, private route: ActivatedRoute) { 
    this.generateEmployeList(100)
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params:any) => {
      this.searchValue = params["search"]
      this.filterDisplayedData(params["search"])
      this.paginate(this.displayedEmployeList,params["page"], params["perPage"])
    });
  }

  generateEmployeList (n: number) {
    let tempEmployee : employee[] = []
    for(let i=0;i<n;i++){
      tempEmployee.push(
        {
          userName: `user-${i+1}`,
          firstName:`firstname-${i+1}`,
          lastName:`lastname-${i+1}`,
          email:`user-${i+1}@email.com`,
          birthDate: new Date("July 21, 1983 01:15:00").toLocaleString(),
          basicSalary:this.randomNumFromInterval(50,500),
          status:i%2 == 0? "active" : "inactive",
          group:i%2 == 0? "a" : "b",
          description: new Date("July 21, 1983 01:15:00").toLocaleString(),
         }
      )
    }
    this.employeeList = tempEmployee
  }
  ToEmployeeDetailPage(data:employee) {
    this.router.navigate([`employee/detail/${data.userName}`],{queryParams: data});
  }
  ToAddEmployeePage() {
    this.router.navigate(['employee/add']);
  }
  randomNumFromInterval(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min)*100000;
  }
  stopPropagation(e:Event){
    e.stopPropagation()
  }
  callDialog(action:string, id: string){
    this.modalData = {
      action : action,
      id: id
    }
    this.toogleModal()
  }
  toogleModal(){
    this.showModal = !this.showModal;
  }
  onSearchChange(event: Event) {
    clearTimeout(this.debounceTimer)
    this.debounceTimer = setTimeout(() => {
      if((event.target as HTMLInputElement).value){
        this.router.navigate(['/employee'],{queryParams: 
          {
            search: (event.target as HTMLInputElement).value,
            page: 1,
            perPage: this.paginationState.perPage
          }})
      }else{
        this.router.navigate(['/employee'],{queryParams: 
          {
            page: 1,
            perPage: this.paginationState.perPage
          }})
      }
    }, 500)
  }
  filterDisplayedData (keyword : string){
    if(keyword){
      this.displayedEmployeList = this.employeeList.filter(function (em) {
        return em.userName.includes(keyword) ||
               em.firstName.includes(keyword) ||
               em.lastName.includes(keyword) ||
               em.email.includes(keyword) ||
               em.status.includes(keyword) ||
               em.group.includes(keyword);
      });
    }else{
      this.displayedEmployeList = this.employeeList
    }
    
  }
  paginate (employeeList : employee [], page = 1, perPage = 10){
    const offset = perPage * (page - 1);
    const totalPages = Math.ceil(employeeList.length / perPage);
    const paginatedItems = employeeList.slice(offset, perPage * page);
  
    this.paginationState = 
    {
        total: employeeList.length,
        totalPages: totalPages,
        currentPage:page,
        perPage: perPage,
        firstPage: page == 1,
        lastPage: page == totalPages
    };

    this.displayedEmployeList = paginatedItems;
  };
  onPerPageChange(value:string): void {
		this.router.navigate(['/employee'],{queryParams: 
      {
        search: this.searchValue,
        page: 1,
        perPage: value
      }})
	}
  onPageChange(value:string){
    if(value === 'prev'){
      this.router.navigate(['/employee'],{queryParams: 
        {
          search: this.searchValue,
          page: Number(this.paginationState.currentPage) - 1,
          perPage: this.paginationState.perPage
        }})
    }else{
      this.router.navigate(['/employee'],{queryParams: 
        {
          search: this.searchValue,
          page: Number(this.paginationState.currentPage) + 1,
          perPage: this.paginationState.perPage
        }})
    }
  }
  sort(colName:any) {
    if(colName === this.sortState.column){
      if(this.sortState.asc){
        this.employeeList.sort((a : any, b : any) => a[colName] < b[colName] ? 1 : a[colName] > b[colName] ? -1 : 0)
      }else{
        this.employeeList.sort((a : any, b : any) => a[colName] > b[colName] ? 1 : a[colName] < b[colName] ? -1 : 0)
      }
      this.sortState.asc = !this.sortState.asc
    }else{
      this.employeeList.sort((a : any, b : any) => a[colName] > b[colName] ? 1 : a[colName] < b[colName] ? -1 : 0)
      this.sortState.column = colName
      this.sortState.asc = true
    }
    this.filterDisplayedData(this.searchValue)
    this.paginate(this.displayedEmployeList,1, this.paginationState.perPage)
}
}
export class employee {
  userName:string = ""
  firstName: string = "";
  lastName: string = "";
  email: string = "";
  birthDate: string = "";
  basicSalary: number = 0;
  status: string = "";
  group: string = "";
  description: string = "";
}
export class modalData {
  id: string = '';
  action: string = '';
}
export class sortState {
  column: string = "";
  asc: boolean  = true;
}
