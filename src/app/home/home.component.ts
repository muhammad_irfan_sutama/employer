import { Component, OnInit } from '@angular/core';
import { RouterOutlet, Router } from '@angular/router';
@Component({
  selector: 'app-home',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit{
  constructor(private router: Router) { 
  }
  ngOnInit(): void {
    if(!localStorage.getItem("username")){
      this.ToLoginPage();
    }
  }

  ToLoginPage() {
    this.router.navigate(['login']);
    localStorage.clear();
  }
}
