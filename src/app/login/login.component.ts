import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  userForm: FormGroup = new FormGroup({
    userName: new FormControl("",[Validators.required]),
    password: new FormControl("",[Validators.required]),
  });
  isFormSubmitted: boolean = false;

  constructor(private router: Router) { 
  }
  onSubmit() {
    if (this.userForm.valid) {
      if(this.userForm.value.userName === "admin" && this.userForm.value.password === "admin"){
        localStorage.setItem("username", "admin");
        this.ToEmployeeListPage();
      }else{
        alert("Invalid Credentials")
      }
    }else{
      alert("Please Input Password and Username")
    }
  }
  
  ToEmployeeListPage() {
    this.router.navigate(['employee']);
  }
}
